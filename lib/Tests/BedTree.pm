package BedTree;

use warnings;
use strict;
use Tree::Interval;


sub new
{
    my $class = shift;
    
    my $self = {};
    bless ($self, $class);
    
    $self->{tree} = Tree::Interval->new();
    
    
    return $self;
}

sub run
{
    my $self = shift;
    my $query = shift;
    
    # insert data to tree
    $self->{tree}->insert(1,6, ["cat", 1, 10]);
    $self->{tree}->insert(7,10, ["dog", 11, 20]);
    
    # look for position
    my $value = $self->{tree}->find($query);
    
    # print result
    my $result = defined($value) ? $value : ["<empty>", -1, -1];
    print join("\t",@{$result}),"\n";
    
}

1; # returns true

