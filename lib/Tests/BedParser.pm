package BedParser;

use warnings;
use strict;
use IO::Zlib;
use Benchmark;

use lib "$FindBin::Bin/lib";
use Record::Bed;

# constructor
sub new
{
    my $class = shift;
    my $filename = shift;
    
    my $self = {};
    bless ($self, $class);
    
    # check if compressed file
    my $fileHandle = ();
    if ($filename =~ m/\.gz$/)
    {
        $fileHandle = IO::Zlib->new($filename, "rb");
        if (!defined($fileHandle))
        {
            $fileHandle->close();
            die $!;
        }
        
    }
    else
    {
        open($fileHandle, "<", $filename) or die $!;
    }
    
    $self->{handle} = $fileHandle;
    
    return $self;
}

# destructor
sub DESTROY {
    my $self = shift;
    $self->{handle}->close() if $self->{handle};
}

# read
sub read
{
    my $self = shift;
    
    my $line = readline($self->{handle});
    
    return $line;
}

# parse
sub parse
{
    my $self = shift;
    my $count = 0;
    
    my $t0 = Benchmark->new();
    
    while(my $line = $self->read())
    {
        chomp($line);
        
        # create bed object
        my $bed = Bed->new($line);
        
        # random base
        my $leftRange = ($bed->{chromStart} - 1000);
        my $rightRange = ($bed->{chromEnd} - $bed->{chromStart}) + 2000;
        my $randomBase = int(rand($rightRange)) + $leftRange;
        
        # process bed hit
        my ($hit, $feature) = $bed->closest($randomBase);
        
        # pass regions
        
        print $bed->{name},"\t",$feature,"\t",$bed->{thickEndNode}[0],"\n";

        
        $count++;
        last if($count == 100);
    }
    my $t1 = Benchmark->new();
    my $td = timediff($t1, $t0);
    print "benchmark :: ",timestr($td),"\n";
    print $count,"\n";
    
}


1; # return true