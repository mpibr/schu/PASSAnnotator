
my $line = 'chr1	388172	401149	NM_001099460;Vom2r3	0	+	388172	401149	0	6	206,283,804,225,124,884,	0,2845,3556,5212,10804,12093,';


# ... or use subroutine references.
=head
 cmpthese(1000000, {
 'Split' => '&parseLineOld($line)',
 'Substr' => '&parseLineNew($line)',
 });
 =cut


my $res = mysplit($line, "\t", 12);
my $ex = mysplit($res->[-1], ",",$res->[9]);
print join(";", @{$res}),"\n";
print join(";", @{$ex}),"\n";



sub parseLineOld()
{
    my $line = 'chr1	388172	401149	NM_001099460;Vom2r3	0	+	388172	401149	0	6	206,283,804,225,124,884,	0,2845,3556,5212,10804,12093,';
    my @res = split('\t', $line, 12);
    return \@res;
}


sub parseLineNew()
{
    my $line = 'chr1	388172	401149	NM_001099460;Vom2r3	0	+	388172	401149	0	6	206,283,804,225,124,884,	0,2845,3556,5212,10804,12093,';
    my @res = (0) x 12;
    my $pos_prev = -1;
    my $pos_next = 0;
    my $line_siz = length($line);
    my $i = 0;
    while($pos_next < $line_siz)
    {
        $pos_next = index($line,"\t", $pos_prev + 1);
        $pos_next = $line_siz if($pos_next == -1);
        my $txt = substr($line, $pos_prev + 1, $pos_next - $pos_prev - 1);
        $res[$i] = $txt;
        
        #print $i,"\t",$txt,"\n";
        
        $i++;
        $pos_prev = $pos_next;
    }
    #print $i,"\t",join(";",@res),"\n";
    
    return \@res;
}

sub mysplit($$$)
{
    my $line = $_[0];
    my $delim = $_[1];
    my $count = $_[2];
    
    # allocate result array
    my @vals = (0) x $count;
    my $char_prev = -1;
    my $char_next = -1;
    my $line_siz = length($line);
    my $idx = 0;
    while ($char_next < $line_siz)
    {
        # find delimiter
        $char_next = index($line, $delim, $char_prev + 1);
        $char_next = $line_siz if($char_next == - 1);
        
        # splice line
        my $str = substr($line, $char_prev + 1, $char_next - $char_prev - 1);
        
        # fill array
        $vals[$idx] = $str;
        
        # update counters
        $idx++;
        $char_prev = $char_next;
        
    }
    
    return \@vals;
}




#print $line,"\n",index($line,"\t"),"\n";
#print $cnt,"\n";

