package Pass;

use warnings;
use strict;

sub new
{
    my $class = shift;
    my $pass_line = shift;
    
    my $self = {};
    bless ($self, $class);
    

    # parse line
    my @pass_vals = split("\t", $pass_line, 15);
    
    # properties
    $self->{chrom} = $pass_vals[0];
    $self->{windowStart} = $pass_vals[1];
    $self->{windowEnd} = $pass_vals[2];
    $self->{name} = $pass_vals[3];
    $self->{span} = $pass_vals[4];
    $self->{strand} = $pass_vals[5];
    $self->{masked} = $pass_vals[6];
    $self->{readsCountBases} = $pass_vals[7];
    $self->{readsSumCoverage} = $pass_vals[8];
    $self->{readsMaxCoverage} = $pass_vals[9];
    $self->{readsBestBase} = $pass_vals[10];
    $self->{tailsCountBases} = $pass_vals[11];
    $self->{tailsSumCoverage} = $pass_vals[12];
    $self->{tailsMaxCoverage} = $pass_vals[13];
    $self->{tailsBestBase} = $pass_vals[14];

    return $self;
}



sub ispoly
{
    my $self = shift;
    my $ispoly = ($self->{tailsCountBases} > 0) ? 1 : 0;
    return $ispoly;
}

sub ischrom
{
    my $self = shift;
    my $chrom = shift;
    my $ischrom = ($self->{chrom} eq $chrom) ? 1 : 0;
    return $ischrom;
}

sub window
{
    my $self = shift;
    my $window_upstream = shift;
    
    # define query region
    my $queryLeft = ($self->{tailsBestBase} - $window_upstream);
    my $queryRight = $self->{tailsBestBase};
    
    # fix negative strand
    if ($self->{strand} eq "-")
    {
        $queryLeft = $self->{tailsBestBase};
        $queryRight = ($self->{tailsBestBase} + $window_upstream);
    }
    
    # compensate negative
    $queryLeft = 0 if($queryLeft < 0);
    
    # return query region
    return $self->{chrom} . ":" . $queryLeft . "-" . $queryRight;
}

sub regions
{
    my $self = shift;
    my $bed = shift;
    my $baseNode = shift;
    my $feature = shift;
    
}

1; # return true